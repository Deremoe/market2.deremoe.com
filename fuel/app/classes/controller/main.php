<?php
/**
 * Created by PhpStorm.
 * User: aldri
 * Date: 30/07/2015
 * Time: 14:17
 */

class Controller_Main extends Controller
{
    public function action_index()
    {
        return Response::forge(View::forge('main'));
    }
}