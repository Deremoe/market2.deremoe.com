<!DOCTYPE html>
<html>
<head lang="en"></head>
<meta charset="UTF-8"/>
<meta name="viewport"
      content="width=device-width,
               height=device-width,
               initial-scale=1.0,
               maximum-scale=1.0,
               user-scalable=no">
<link href='http://fonts.googleapis.com/css?family=Roboto:500,900,300,700,400' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/uikit/2.21.0/css/uikit.min.css"/>
<?php print Asset::css('dm-grid.css'); ?>
<?php print Asset::css('core.css');?>
<?php print Asset::css('directives.css');?>
<?php print Asset::css('main.css');?>
<link rel="stylesheet"
      ng-href="/assets/css/{{templateCss}}"/>
<body ng-controller="mainCtrl">
<nav id="dm-main-nav" ng-show="showNav">
    <div id="brand">
        Deremoe Market
    </div>
    <div id="nav">
        <a href="/#/add" ng-show="showMenu">
            <i class="uk-icon uk-icon-cart-plus"></i>
            ADD
        </a>
        <a href="#dm-main-menu"
           ng-show="showMenu"
           class="uk-text-center"
           data-uk-offcanvas>
            <i class="uk-icon uk-icon-navicon"></i>
            MENU
        </a>
        <a href="/#/" ng-show="!showMenu">
            <i class="uk-icon uk-icon-arrow-left"></i>
            BACK TO MAIN
        </a>
    </div>
</nav>
<main ng-view></main>

<!-- Main offcanvas -->
<div id="dm-main-menu"
     class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
    <ul class="uk-nav uk-nav-offcanvas"
        data-nav>
        <li class="uk-nav-header">Admin Controls</li>
        <li><a href="/#/admin/users">User Administration</a></li>

        <li class="uk-nav-header">User Profiles</li>
        <li><a href="/#/user/profile">User Profiles</a></li>
        <li><a href="/#/user/feedback">Feedback</a></li>

        <li class="uk-nav-header">Selling Controls</li>
        <li><a href="/#/browse/own">Own Items</a></li>
        <li><a href="/#/add">Add Item</a></li>
    </ul>
    </div>
</div>

<script
    data-main="app/main.js"
    src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.17/require.min.js">
</script>
</div>
</body>
</html>