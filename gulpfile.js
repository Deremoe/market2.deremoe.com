/**
 * Created by aldri on 30/07/2015.
 */
var gulp        = require('gulp');
/*
    Javascript
 */
var coffee      = require('gulp-coffee');
var ngAnnotate  = require('gulp-ng-annotate');
/*
    CSS
 */
var less        = require('gulp-less');
var prefixer    = require('gulp-autoprefixer');
var combineCSS  = require('combine-css');
var minifyCSS   = require('gulp-minify-css');
/*
    HTML
 */
var jade        = require('gulp-jade');

/*
    Essential
 */
var plumber     = require('gulp-plumber');

/*
    Run-once
 */

gulp.task('coffee-main',function(){
    gulp.src('_public/app/**/*.coffee')
        .pipe(plumber())
        .pipe(coffee({bare: true}))
        .pipe(ngAnnotate({single_quotes:true}))
        .pipe(gulp.dest('public/app'));
});

gulp.task('less-main',function(){
   gulp.src('_public/assets/less/**/*.less')
       .pipe(plumber())
       .pipe(less())
       /*
       .pipe(combineCSS({
           lengthLimit:256,
           prefix:'dm-main-'
       }))
       */
       .pipe(prefixer())
       .pipe(minifyCSS())
       .pipe(gulp.dest('public/assets/css'));
});

gulp.task('jade',function(){
   gulp.src('_public/app/views/**/*.jade')
       .pipe(plumber())
       .pipe(jade())
       .pipe(gulp.dest('public/app/views'));
});

/*
    Run-watch
 */
gulp.task('watch',function(){
    gulp.watch('_public/app/**/*.coffee',['coffee-main']);
    gulp.watch('_public/assets/less/**/*.less',['less-main']);
    gulp.watch('_public/app/views/**/*.jade',['jade'])
});



gulp.task('default',['coffee-main','less-main','watch','jade']);