define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.controller('mainCtrl', ['$rootScope', '$q', '$scope', function($rootScope, $q, $scope) {
    var def;
    $scope.showMenu = false;
    $scope.showNav = false;
    $scope.$on('showNav', function(event, data) {
      return $scope.showNav = data.show;
    });
    $scope.$on('showMenu', function(event, data) {
      return $scope.showMenu = data.show;
    });
    def = $q.defer;
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
      return require(['uiKit']);
    });
    return def.promise;
  }]);
});
