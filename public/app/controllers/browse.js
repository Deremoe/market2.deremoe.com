define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('browseCtrl', function($scope, helper) {
    helper.setCss('views/browser.css');
    $scope.$emit('showNav', {
      show: true
    });
    $scope.$emit('showMenu', {
      show: true
    });
    return console.log("this controller ran");
  });
});
