define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('splashCtrl', function($scope, helper) {
    helper.setCss('views/splash.css');
    $scope.message = "this is a new message";
    $scope.$emit('showNav', {
      show: false
    });
    return $scope.$emit('showMenu', {
      show: false
    });
  });
});
