define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('addCtrl', function($scope, helper) {
    helper.setCss('views/add.css');
    $scope.$emit('showNav', {
      show: true
    });
    $scope.$emit('showMenu', {
      show: false
    });
    $scope.selectedRegion = null;
    $scope.region = [
      {
        name: 'Rizal',
        value: '1'
      }, {
        name: 'NCR',
        value: '2'
      }, {
        name: 'Pangasinan',
        value: '3'
      }
    ];
    $scope.attIsUsed = false;
    $scope.attShip = false;
    $scope.attMeet = false;
    $scope.isNeg = false;
    $scope.isTrade = false;
    $scope.category = null;
    $scope.categories = [
      {
        name: 'Category 1',
        value: '1'
      }, {
        name: 'Category 2',
        value: '2'
      }, {
        name: 'Category 3',
        value: '3'
      }, {
        name: 'Category 4',
        value: '4'
      }, {
        name: 'Category 5',
        value: '5'
      }
    ];
    return $scope.seeRegion = function() {
      return console.log($scope.selectedRegion);
    };
  });
});
