define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('adminUserCtrl', function($scope, helper) {
    helper.setCss('admin/user.css');
    $scope.$emit('showNav', {
      show: true
    });
    $scope.$emit('showMenu', {
      show: true
    });
    return console.log("this controller ran");
  });
});
