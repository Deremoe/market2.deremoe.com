define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('authCtrl', function($scope, helper) {
    helper.setCss('views/auth.css');
    $scope.message = "Auth Ctrl";
    $scope.$emit('showNav', {
      show: true
    });
    return $scope.$emit('showMenu', {
      show: false
    });
  });
});
