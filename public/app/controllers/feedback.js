define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('feedbackCtrl', function($scope, helper) {
    helper.setCss('views/feedback.css');
    $scope.$emit('showNav', {
      show: true
    });
    return $scope.$emit('showMenu', {
      show: false
    });
  });
});
