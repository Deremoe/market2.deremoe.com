define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('itemCtrl', function($scope, helper) {
    helper.setCss('views/item.css');
    $scope.$emit('showNav', {
      show: true
    });
    return $scope.$emit('showMenu', {
      show: false
    });
  });
});
