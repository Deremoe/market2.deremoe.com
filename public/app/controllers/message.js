define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.registerController('messageCtrl', function($scope, helper) {
    helper.setCss('user/message.css');
    $scope.$emit('showNav', {
      show: true
    });
    $scope.$emit('showMenu', {
      show: false
    });
    return console.log("controller loaded");
  });
});
