define(function() {
  var initFunc;
  initFunc = function(controller) {
    return [
      '$q', function($q) {
        var def;
        def = $q.defer();
        require(controller, function() {
          return def.resolve();
        });
        return def.promise;
      }
    ];
  };
  return [
    '$routeProvider', function($rp) {
      $rp.when('/', {
        controller: 'splashCtrl',
        templateUrl: 'app/views/splash.html',
        resolve: {
          load: initFunc(['controllers/splash'])
        }
      });
      $rp.when('/auth', {
        controller: 'authCtrl',
        templateUrl: 'app/views/auth.html',
        resolve: {
          load: initFunc(['controllers/auth'])
        }
      });
      $rp.when('/browse', {
        controller: 'browseCtrl',
        templateUrl: 'app/views/browse.html',
        resolve: {
          load: initFunc(['controllers/browse'])
        }
      });
      $rp.when('/add', {
        controller: 'addCtrl',
        templateUrl: 'app/views/add.html',
        resolve: {
          load: initFunc(['controllers/add'])
        }
      });
      $rp.when('/item', {
        controller: 'itemCtrl',
        templateUrl: 'app/views/item.html',
        resolve: {
          load: initFunc(['controllers/item'])
        }
      });
      $rp.when('/user/feedback', {
        controller: 'feedbackCtrl',
        templateUrl: 'app/views/feedback.html',
        resolve: {
          load: initFunc(['controllers/feedback'])
        }
      });
      $rp.when('/user/message', {
        controller: 'messageCtrl',
        templateUrl: 'app/views/message.html',
        resolve: {
          load: initFunc(['controllers/message'])
        }
      });
      $rp.when('/admin/user', {
        controller: 'adminUserCtrl',
        templateUrl: 'app/views/admin/user.html',
        resolve: {
          load: initFunc(['controllers/admin/user'])
        }
      });
      return $rp.when('/user/profile', {
        controller: 'userProfileCtrl',
        templateUrl: 'app/views/admin/user.profile.html',
        resolve: {
          load: initFunc(['controllers/admin/profile'])
        }
      });
    }
  ];
});
