define(['services', 'directives', 'router'], function(services, directives, router) {
  var appMain, configs;
  configs = ['ngRoute', 'ngAnimate'];
  appMain = angular.module('appMarket', configs);
  appMain.config([
    '$controllerProvider', function($cp) {
      return appMain.registerController = $cp.register;
    }
  ]);
  appMain.config(router);
  return require(services, function() {
    return require(directives, function() {
      return require(['controllers/main'], function() {
        return angular.bootstrap(document, ['appMarket']);
      });
    });
  });
});
