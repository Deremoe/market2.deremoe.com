require.config({
  baseUrl: 'app/',
  paths: {
    angular: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular.min',
    ngRoute: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-route.min',
    ngAnimate: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-animate.min',
    router: 'config/angular.routes',
    directives: 'directives/ref',
    services: 'services/ref',
    coreModule: 'config/angular.module',
    jQuery: '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min',
    uiKit: '//cdnjs.cloudflare.com/ajax/libs/uikit/2.21.0/js/uikit',
    wysiwig: 'vendor/trumbowyg',
    clamp: 'vendor/clamp'
  },
  shim: {
    ngRoute: {
      deps: ['angular']
    },
    ngAnimate: {
      deps: ['ngRoute', 'clamp']
    },
    coreModule: {
      deps: ['ngAnimate']
    },
    uiKit: {
      deps: ['jQuery']
    },
    wysiwig: {
      deps: ['jQuery']
    }
  }
});

define(['coreModule'], function() {});
