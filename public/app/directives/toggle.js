define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.directive('toggle', function() {
    var link, returnMe;
    link = function(scope, elem, attr) {
      return scope.setToggle = function() {
        return scope.toggle = !scope.toggle;
      };
    };
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {
        name: '@',
        toggle: '='
      },
      templateUrl: 'app/views/directives/toggle.html',
      link: link
    };
  });
});
