define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.directive('dropSearch', function() {
    var link, returnMe;
    link = function(scope, elem, attr) {
      scope.filterItem = {
        name: ''
      };
      scope.showDialog = false;
      scope.showDialogBox = function() {
        return scope.showDialog = !scope.showDialog;
      };
      return scope.setSelected = function(item) {
        scope.filterItem.name = item.name;
        scope.selected = item;
        return scope.showDialog = false;
      };
    };
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {
        selected: '=',
        selection: '='
      },
      templateUrl: 'app/views/directives/search-dropdown.html',
      link: link
    };
  });
});
