var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  appMain.directive('item', function() {
    var returnMe;
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {
        data: '=data'
      },
      templateUrl: 'app/views/directives/market/browse.html'
    };
  });
  appMain.directive('categories', function() {
    var link, returnMe;
    link = function(scope, elem, attr) {
      var setActives;
      scope.isActive = [];
      setActives = function() {
        var category, i, j, len, ref, results;
        i = 0;
        ref = scope.info;
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
          category = ref[j];
          scope.isActive[i] = false;
          results.push(i++);
        }
        return results;
      };
      scope.setActive = function(key, value) {
        setActives();
        scope.category = value;
        return scope.isActive[key] = true;
      };
      return setActives();
    };
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {
        category: '=',
        info: '='
      },
      templateUrl: 'app/views/directives/market/category.html',
      link: link
    };
  });
  appMain.directive('uploadFiles', function() {
    var link, returnMe;
    link = function(scope, elem, attr) {
      var checkSize, dragOverOrEnter, isTypeValid, params;
      params = {
        size: 3,
        limit: 5,
        mime: ['image/jpg', 'image/jpeg']
      };
      scope.isActive = [];
      scope.isDragDropPresent = false;
      dragOverOrEnter = function(event) {
        if (event != null) {
          event.preventDefault();
        }
        event.dataTransfer.effectAllowed = 'copy';
        return false;
      };
      scope.deleteFile = function(key) {
        scope.validFiles.splice(key, 1);
        if (scope.validFiles.length === 0) {
          return scope.invalidFiles = [];
        }
      };
      if (window.FileReader != null) {
        scope.isDragDropPresent = true;
        scope.filesPresent = false;
        scope.validFiles = [];
        scope.invalidFiles = [];
        checkSize = function(size) {
          if (params.size < (size * 1024)) {
            return true;
          } else {
            return false;
          }
        };
        isTypeValid = function(type) {
          if (indexOf.call(params.mime, type) >= 0) {
            return true;
          } else {
            return false;
          }
        };
        elem.bind('dragover', dragOverOrEnter);
        elem.bind('dragenter', dragOverOrEnter);
        return elem.bind('drop', function(event) {
          var counter, files;
          if (event != null) {
            event.preventDefault();
          }
          files = event.dataTransfer.files;
          counter = 0;
          scope.validFiles = [];
          scope.invalidFiles = [];
          angular.forEach(files, function(value, key) {
            var reader;
            if (checkSize(value.size) && isTypeValid(value.type) && counter < params.limit) {
              reader = new FileReader();
              reader.onload = function(evt) {
                return value.imageUrl = evt.target.result;
              };
              value.imageFile = reader.readAsDataURL(value);
              scope.validFiles.push(value);
            } else {
              scope.invalidFiles.push(value);
            }
            return counter++;
          });
          scope.$apply(function() {
            scope.validFiles;
            return scope.invalidFiles;
          });
          console.log(scope.validFiles);
          return false;
        });
      }
    };
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {},
      templateUrl: 'app/views/directives/market/upload.html',
      link: link
    };
  });
  appMain.directive("searchBar", ['$http', function($http) {
    var link, returnMe;
    link = function(scope, elem, attr) {
      scope.attIsUsed = false;
      scope.attShip = false;
      scope.attMeet = false;
      scope.isNeg = false;
      scope.isTrade = false;
      scope.region = [
        {
          name: 'Rizal',
          value: '1'
        }, {
          name: 'NCR',
          value: '2'
        }, {
          name: 'Pangasinan',
          value: '3'
        }
      ];
      scope.categories = [
        {
          name: 'Category 1',
          value: '1'
        }, {
          name: 'Category 2',
          value: '2'
        }, {
          name: 'Category 3',
          value: '3'
        }, {
          name: 'Category 4',
          value: '4'
        }, {
          name: 'Category 5',
          value: '5'
        }
      ];
      scope.selectedRegion = null;
      scope.searchBarToggle = true;
      return scope.toggleSearchBar = function() {
        console.log("I am running");
        return scope.searchBarToggle = !scope.searchBarToggle;
      };
    };
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {},
      templateUrl: 'app/views/directives/market/search.html',
      link: link
    };
  }]);
  appMain.directive("itemSmall", function() {
    var link, returnMe;
    link = function(scope, elem, attr) {};
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {},
      templateUrl: 'app/views/directives/market/item.html',
      link: link
    };
  });
  appMain.directive("feedback", function() {
    var link, returnMe;
    link = function(scope, elem, attr) {};
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {},
      templateUrl: 'app/views/directives/market/feedback.html',
      link: link
    };
  });
  appMain.directive("mailbag", function() {
    var link, returnMe;
    link = function(scope, elem, attr) {};
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {},
      templateUrl: 'app/views/directives/market/mailbag.html',
      link: link
    };
  });
  return appMain.directive("privateMessage", function() {
    var link, returnMe;
    link = function(scope, elem, attr) {};
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {},
      templateUrl: 'app/views/directives/market/message.box.html',
      link: link
    };
  });
});
