define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  return appMain.directive('wysiwig', function() {
    return {
      link: function(scope, elem, attr) {
        return require(['wysiwig'], function() {
          return $(elem).trumbowyg({
            btns: ['formatting', 'bold', 'italic', 'underline', 'strikethrough', '|', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', '|', 'unorderedList', 'orderedList'],
            fullscreenable: false
          });
        });
      }
    };
  });
});
