define(function() {
  var appMain;
  appMain = angular.module('appMarket');
  appMain.directive('panel', function() {
    var returnMe;
    return returnMe = {
      restrict: 'E',
      replace: true,
      scope: {},
      transclude: true,
      templateUrl: 'app/views/directives/panel.html'
    };
  });
  appMain.directive('panelHeader', function() {
    var returnMe;
    return returnMe = {
      require: '^panel',
      replace: true,
      scope: {},
      transclude: true,
      templateUrl: 'app/views/directives/panel-header.html'
    };
  });
  return appMain.directive('panelBody', function() {
    var returnMe;
    return returnMe = {
      require: '^panel',
      replace: true,
      scope: {},
      transclude: true,
      templateUrl: 'app/views/directives/panel-body.html'
    };
  });
});
