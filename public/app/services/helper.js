define(function(){
    var appMain = angular.module('appMarket');
    appMain.factory('helper',['$location','$rootScope','$http',function($loc,$rs,$http){
        return {
            getLocation:function(){
                return $loc.path();
            },
            setLocation:function(path){
                return $loc.path(path);
            },
            setCss:function(cssPath){
                $rs.templateCss = cssPath;
            },
            setNav:function(){
                $rs.showNav = true;
            },
            releaseNav:function(){
                $rs.showNav = false;
            },
            httpCall:function(base_url,data){
                var query = $http.post(base_url,data);
                return query;
            },
            reatartNotify:function(){
                
            },
            csrf: function () {
                if (document.cookie.length > 0)
                {
                    var c_name = "dmp_market_csrf";
                    c_start = document.cookie.indexOf(c_name + "=");
                    if (c_start != -1)
                    {
                        c_start = c_start + c_name.length + 1;
                        c_end = document.cookie.indexOf(";" , c_start);
                        if (c_end == -1)
                        {
                            c_end=document.cookie.length;
                        }
                        return unescape(document.cookie.substring(c_start, c_end));
                    }
                }
                return "";
            }
        };
    }]);
});