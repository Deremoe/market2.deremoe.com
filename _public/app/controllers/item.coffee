define ->
  appMain = angular.module 'appMarket'
  appMain.registerController 'itemCtrl', ($scope,helper) ->
    helper.setCss 'views/item.css'

    $scope.$emit 'showNav',{show:true}
    $scope.$emit 'showMenu',{show:false}