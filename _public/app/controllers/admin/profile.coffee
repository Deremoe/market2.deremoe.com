define ->
  appMain = angular.module 'appMarket'
  appMain.registerController 'userProfileCtrl', ($scope,helper) ->
    helper.setCss 'admin/profile.css'

    $scope.$emit 'showNav',{show:true}
    $scope.$emit 'showMenu',{show:true}

    console.log "this controller ran"