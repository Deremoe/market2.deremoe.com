define ->
  appMain = angular.module 'appMarket'
  appMain.registerController 'authCtrl', ($scope,helper) ->
    helper.setCss 'views/auth.css'
    $scope.message = "Auth Ctrl"

    $scope.$emit 'showNav',{show:true}
    $scope.$emit 'showMenu',{show:false}
