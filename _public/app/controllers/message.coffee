define ->
    appMain = angular.module 'appMarket'
    appMain.registerController 'messageCtrl', ($scope,helper) ->
      helper.setCss 'user/message.css'

      $scope.$emit 'showNav',{show:true}
      $scope.$emit 'showMenu',{show:false}

      console.log "controller loaded"