define ->
  appMain = angular.module 'appMarket'
  appMain.registerController 'browseCtrl', ($scope,helper) ->
    helper.setCss 'views/browser.css'

    $scope.$emit 'showNav',{show:true}
    $scope.$emit 'showMenu',{show:true}

    console.log "this controller ran"