define ->
  appMain = angular.module 'appMarket'
  appMain.registerController 'splashCtrl', ($scope,helper) ->
    helper.setCss 'views/splash.css'
    $scope.message = "this is a new message"

    $scope.$emit 'showNav',{show:false}
    $scope.$emit 'showMenu',{show:false}