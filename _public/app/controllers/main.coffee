define ->
  appMain = angular.module 'appMarket'
  appMain.controller 'mainCtrl',($rootScope,$q,$scope) ->
    $scope.showMenu = false
    $scope.showNav  = false

    $scope.$on 'showNav',(event,data) ->
      $scope.showNav = data.show

    $scope.$on 'showMenu',(event,data) ->
      $scope.showMenu = data.show

    def = $q.defer
    $rootScope.$on '$routeChangeStart', (event,next,current) ->
      require ['uiKit']
    def.promise