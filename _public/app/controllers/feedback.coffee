define ->
  appMain = angular.module 'appMarket'
  appMain.registerController 'feedbackCtrl', ($scope,helper) ->
    helper.setCss 'views/feedback.css'

    $scope.$emit 'showNav',{show:true}
    $scope.$emit 'showMenu',{show:false}
