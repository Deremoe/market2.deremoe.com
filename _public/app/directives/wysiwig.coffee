define ->
  appMain = angular.module 'appMarket'
  appMain.directive 'wysiwig', ->
    link:(scope,elem,attr) ->
      require ['wysiwig'], ->
        $(elem).trumbowyg
          btns:[
            'formatting'
            'bold'
            'italic'
            'underline'
            'strikethrough'
            '|'
            'justifyLeft'
            'justifyCenter'
            'justifyRight'
            'justifyFull'
            '|'
            'unorderedList'
            'orderedList'
          ]
          fullscreenable: false