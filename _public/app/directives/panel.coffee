define ->
  appMain = angular.module 'appMarket'
  appMain.directive 'panel', ->
    returnMe =
      restrict:'E'
      replace:true
      scope:{}
      transclude:true
      templateUrl:'app/views/directives/panel.html'

  appMain.directive 'panelHeader', ->
    returnMe =
      require:'^panel'
      replace:true
      scope:{}
      transclude:true
      templateUrl:'app/views/directives/panel-header.html'

  appMain.directive 'panelBody', ->
    returnMe =
      require:'^panel'
      replace:true
      scope:{}
      transclude:true
      templateUrl:'app/views/directives/panel-body.html'