define ->
  appMain = angular.module 'appMarket'
  appMain.directive 'clampText', ->
    returnMe = {
      restrict:'A'
      link: (scope,elem,attr) ->
        $clamp(elem[0],attr.line)
    }