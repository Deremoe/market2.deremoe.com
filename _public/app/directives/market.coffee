##Market Only Directives
define ->
  appMain = angular.module 'appMarket'
  appMain.directive 'item', ->
    returnMe = {
      restrict: 'E'
      replace:true
      scope:{
        data:'=data'
      }
      templateUrl:'app/views/directives/market/browse.html'
    }

  appMain.directive 'categories', ->
    link = (scope,elem,attr) ->
      scope.isActive = []

      setActives = ->
        i = 0
        for category in scope.info
          scope.isActive[i] = false;
          i++

      scope.setActive = (key,value) ->
        setActives()
        scope.category      = value
        scope.isActive[key] = true

      setActives()
    returnMe = {
      restrict  :'E'
      replace   :true
      scope     :{
        category:'='
        info:'='
      }
      templateUrl:'app/views/directives/market/category.html',
      link:link
    }

  appMain.directive 'uploadFiles', ->
    link = (scope,elem,attr) ->
      params = {
        size  :3
        limit :5
        mime  : ['image/jpg','image/jpeg']
      }
      scope.isActive = []
      scope.isDragDropPresent = false

      dragOverOrEnter = (event) ->
        event?.preventDefault()
        event.dataTransfer.effectAllowed = 'copy'
        false

      scope.deleteFile = (key) ->
        scope.validFiles.splice(key,1)
        if scope.validFiles.length is 0
          scope.invalidFiles = []

      if window.FileReader?
        scope.isDragDropPresent = true
        scope.filesPresent      = false
        scope.validFiles  = []
        scope.invalidFiles= []

        checkSize = (size) ->
          if params.size < (size * 1024)
            true
          else
            false

        isTypeValid = (type) ->
          if type in params.mime
            true
          else
            false

        #Process DragOverEvent Prevent File Redirection
        elem.bind 'dragover', dragOverOrEnter
        elem.bind 'dragenter', dragOverOrEnter

        #Bind Element DragOver
        elem.bind 'drop',(event) ->
          event?.preventDefault()
          files = event.dataTransfer.files;

          counter = 0;

          scope.validFiles    = []
          scope.invalidFiles  = []
          angular.forEach files,(value,key) ->
            if checkSize(value.size) and isTypeValid(value.type) and counter < params.limit
              reader = new FileReader()
              reader.onload = (evt) ->
                value.imageUrl = evt.target.result
              value.imageFile = reader.readAsDataURL value
              scope.validFiles.push value
            else
              scope.invalidFiles.push value
            counter++

          scope.$apply ->
            scope.validFiles
            scope.invalidFiles
          console.log scope.validFiles

          #reader.readAsDataURL(file)
          return false

    returnMe = {
      restrict:'E'
      replace:true
      scope:{}
      templateUrl:'app/views/directives/market/upload.html'
      link:link
    }

  appMain.directive "searchBar", ($http) ->
    link = (scope,elem,attr) ->
      scope.attIsUsed = false
      scope.attShip   = false
      scope.attMeet   = false
      scope.isNeg     = false
      scope.isTrade   = false

      scope.region = [
        {name:'Rizal',value:'1'}
        {name:'NCR',value:'2'}
        {name:'Pangasinan',value:'3'}
      ]

      scope.categories= [
        {name:'Category 1',value:'1'}
        {name:'Category 2',value:'2'}
        {name:'Category 3',value:'3'}
        {name:'Category 4',value:'4'}
        {name:'Category 5',value:'5'}
      ];

      scope.selectedRegion = null

      scope.searchBarToggle= true
      scope.toggleSearchBar= ->
        console.log "I am running"
        scope.searchBarToggle = !scope.searchBarToggle


    returnMe = {
      restrict:'E'
      replace :true
      scope   :{}
      templateUrl:'app/views/directives/market/search.html'
      link    :link
    }

  appMain.directive "itemSmall", ->
    link = (scope,elem,attr) ->

    returnMe = {
      restrict: 'E',
      replace : true,
      scope   : {}
      templateUrl:'app/views/directives/market/item.html'
      link: link
    }

  appMain.directive "feedback", ->
    link = (scope,elem,attr) ->

    returnMe = {
      restrict:'E'
      replace :true
      scope   :{}
      templateUrl:'app/views/directives/market/feedback.html'
      link:link
    }

  appMain.directive "mailbag", ->
    link = (scope,elem,attr) ->

    returnMe = {
      restrict:'E'
      replace :true
      scope   :{}
      templateUrl:'app/views/directives/market/mailbag.html'
      link    :link
    }

  appMain.directive "privateMessage", ->
    link = (scope,elem,attr) ->

    returnMe = {
      restrict:'E'
      replace :true
      scope   :{}
      templateUrl:'app/views/directives/market/message.box.html'
      link    :link
    }