define ->
  appMain = angular.module 'appMarket'
  appMain.directive 'toggle', ->
    link = (scope,elem,attr) ->
      scope.setToggle = () ->
        scope.toggle = !scope.toggle
    returnMe =
      restrict:'E'
      replace:true
      scope:
        name:'@'
        toggle:'='
      templateUrl:'app/views/directives/toggle.html'
      link:link