define ->
  appMain = angular.module 'appMarket'
  appMain.directive 'dropSearch', ->
    link = (scope,elem,attr) ->
      scope.filterItem        =
        name:''
      scope.showDialog    = false
      scope.showDialogBox = ->
        scope.showDialog = !scope.showDialog
      scope.setSelected  = (item) ->
        scope.filterItem.name = item.name
        scope.selected = item
        scope.showDialog = false
    returnMe =
      restrict:'E'
      replace:true
      scope:
        selected:'='
        selection:'='
      templateUrl:'app/views/directives/search-dropdown.html'
      link:link