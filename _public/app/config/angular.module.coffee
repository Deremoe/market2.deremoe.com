define ['services','directives','router'], (services,directives,router) ->
  configs = [
    'ngRoute',
    'ngAnimate'
  ]

  appMain = angular.module 'appMarket',configs
  appMain.config ['$controllerProvider',($cp) ->
    appMain.registerController = $cp.register
  ]

  appMain.config router

  require services, ->
    require directives, ->
      require ['controllers/main'],->
        angular.bootstrap document,['appMarket']